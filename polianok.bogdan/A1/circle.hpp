#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

class Circle : public Shape{
public:
	Circle();
  Circle(const point_t point, const double radius);

  double getArea() const;
  double getRadius() const;
  void setRadius(const double radius);
  void getInformation();
  rectangle_t getFrameRect();

  void move(const struct point_t point);
  void move(const double dx, const double dy);

private:
  double radius_ ;
};

#endif
