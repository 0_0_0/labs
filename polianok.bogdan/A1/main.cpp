#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"

int main()
{
  point_t point{0,0};
  Circle circle(point,10);
  circle.getInformation();
  std::cout << "Move on (13,10); \n \n";
  circle.move(13,10);
  circle.getInformation();

  Rectangle rectangle (point,10,20);
  rectangle.getInformation();
  std::cout << "Move to point (5,-30); \n \n";
  point = {5,-30};
  rectangle.move(point);
  rectangle.getInformation();

  return 0;
}
