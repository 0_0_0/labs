#include "shape.hpp"

Shape::Shape (const point_t point)
	:position_(point){}

double Shape::getPosX() const
{
	return position_.x;
}

double Shape::getPosY() const
{
	return position_.y;
}
