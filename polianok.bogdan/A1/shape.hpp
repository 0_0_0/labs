#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

class Shape{
public:
  Shape (const point_t);

  virtual double getArea() const = 0;
  virtual rectangle_t getFrameRect() = 0;
  double getPosX() const;
  double getPosY() const;

  virtual void getInformation()= 0;

  virtual void move(const  point_t point) = 0;
  virtual void move(const double dx, const double dy) = 0;

protected:
  point_t position_;
};

#endif
