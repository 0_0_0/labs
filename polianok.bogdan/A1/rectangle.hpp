#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

class Rectangle : public Shape{
public:
	Rectangle();
  Rectangle(const point_t point, const double x, const double y);

  double getArea() const;
	double getWidth() const;
	double getHeight() const;
	void setWidth(const double width);
	void setHeight(const double height);
	void getInformation();
  rectangle_t getFrameRect();

  void move(const point_t point);
  void move(const double dx, const double dy);

private:
  double width_;
  double height_;
};

#endif
