#include "rectangle.hpp"
#include <iostream>

Rectangle::Rectangle()
  :Shape({0,0}), width_(1), height_(1){}

Rectangle::Rectangle(const point_t point, const double width, const double height)
  : Shape(point), width_(width), height_(height)
  {
    if (width <= 0 || height <= 0){
      std::cerr << "Data is incorrect, width and height will be replaced by 1" << std::endl;
      width_=1;
      height_=1;
    }
  }

double Rectangle::getArea() const
{
  return (width_ * height_);
}

double Rectangle::getWidth() const
{
  return width_;
}

double Rectangle::getHeight() const
{
  return height_;
}

void Rectangle::setWidth(const double width)
{
  if (width <= 0){
      std::cerr << "Width is incorrect, it will be replaced by 1" << std::endl;
      width_=1;
  } else {
    width_ = width;
  }
}

void Rectangle::setHeight(const double height)
{
  if (height_ <= 0){
      std::cerr << "Height is incorrect, it will be replaced by 1" << std::endl;
      height_=1;
  } else {
    height_ = height;
  }
}

void Rectangle::getInformation ()
{
  std::cout << "Rectangle width - " << width_ << "; \n";
  std::cout << "Rectangle height - " << height_ << "; \n";
  std::cout << "Rectangle area - " << getArea() << "; \n";
  std::cout << "Rectangle position - (" << position_.x
    << "," << position_.y << "); \n";
  std::cout << "Rectangle around rectangle: \n";
  std::cout << "  width - " << getFrameRect().width << "; \n";
  std::cout << "  height - " << getFrameRect().height << "; \n";
  std::cout << "  position - (" << getFrameRect().pos.x << ","
    << getFrameRect().pos.y << "); \n \n";
}

rectangle_t Rectangle::getFrameRect()
{
  rectangle_t rectangle;
  rectangle.width = width_;
  rectangle.height = height_;
  rectangle.pos = position_;
  return rectangle;
}

void Rectangle::move(const point_t point)
{
  position_ = point;
}

void Rectangle::move(const double dx, const double dy)
{
  position_.x = position_.x + dx;
  position_.y = position_.y + dy;
}
